let http = require('http');
let port = 3000;
let client ='Aubrey';

http.createServer(function(request , response){
	if (request.url === '/greetings') {
		response.write(`Kamusta ka ${client}?`);
		response.end();

	 } else if (request.url === '/register') {
	 	response.write('Mangyaring magparehistro');
	 	response.end();

	 } else if (request.url === '/login') {
	 	response.write('Lumikha ng iyong account dito');
	 	response.end();

	 }else {
	 	// send this response to the client if the condition resulted to False
	 	response.write('Page was Not Found!')
	 	response.end();
	 }

}).listen(port);

console.log(`Server is running with nodemon on port: ${port}`);
